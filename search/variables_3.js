var searchData=
[
  ['data_384',['data',['../classTouchpad__Driver_1_1TouchPanelDriver.html#a27ef486432a8257c10396069e1f85a27',1,'Touchpad_Driver::TouchPanelDriver']]],
  ['data_5ftask_385',['Data_Task',['../main_8py.html#acefc00b94f654b28be10239c8b906cd6',1,'main']]],
  ['datafromdatatask_386',['dataFromDataTask',['../PC__Frontend_8py.html#a876776a136e8662d4edf2fa6323d6178',1,'PC_Frontend']]],
  ['datatosave_387',['dataToSave',['../classCNTRL__Task_1_1CNTRL__Task.html#a4820d422d798c3eb9632a7a9af7111f7',1,'CNTRL_Task.CNTRL_Task.dataToSave()'],['../classData__Task_1_1Data__Task.html#a5315f0c1a400baefd8993bc7ec82ba87',1,'Data_Task.Data_Task.dataToSave()'],['../PC__Frontend_8py.html#ab2c5b02597446638b86c28f8dd89d120',1,'PC_Frontend.dataToSave()']]],
  ['debug_388',['Debug',['../classCNTRL__Task_1_1CNTRL__Task.html#a1ea9c505a97f110993f329c31bc5c979',1,'CNTRL_Task.CNTRL_Task.Debug()'],['../classEncoder__Driver_1_1Encoder.html#aac52eab5086fecdf82d0499005867c19',1,'Encoder_Driver.Encoder.Debug()'],['../classEncoder__Task_1_1Encoder__Task.html#a694af931bb79d82798ed76b54f7212da',1,'Encoder_Task.Encoder_Task.Debug()'],['../classMotor__Driver_1_1Motor.html#ab56f7e46a3b4e6d24e92fd6fa426cbcd',1,'Motor_Driver.Motor.Debug()'],['../classMotor__Task_1_1Motor__Task.html#a61bcc717237c8961a86693a966015d85',1,'Motor_Task.Motor_Task.Debug()'],['../classTouchpad__Driver_1_1TouchPanelDriver.html#aebafadda1824db14f03048346a5e840f',1,'Touchpad_Driver.TouchPanelDriver.Debug()'],['../classTouchpad__Task_1_1Touchpad__Task.html#aeb6027e5c825e36fb877d61ffc29fc30',1,'Touchpad_Task.Touchpad_Task.Debug()'],['../classUI__Task_1_1UI__Task.html#a758a19d9f68894fa8196c957bc416bc5',1,'UI_Task.UI_Task.Debug()']]],
  ['dx_389',['dx',['../classTouchpad__Driver_1_1TouchPanelDriver.html#a5dfb23383ebf4cddf64892d6704cf49d',1,'Touchpad_Driver::TouchPanelDriver']]],
  ['dy_390',['dy',['../classTouchpad__Driver_1_1TouchPanelDriver.html#aef152d5b6b7972e3bdc194db258b7461',1,'Touchpad_Driver::TouchPanelDriver']]]
];
