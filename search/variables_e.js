var searchData=
[
  ['sensor_448',['sensor',['../main0X04__ME405_8py.html#ab2f6d61127522a6ee42b87df95c305e5',1,'main0X04_ME405.sensor()'],['../MCP9808_8py.html#a7ec530fc4202fa219281770a1a1ac934',1,'MCP9808.sensor()']]],
  ['ser_449',['ser',['../PC__Frontend_8py.html#a3d3ccbcdf27a9900cf0deb04ee87e6bb',1,'PC_Frontend.ser()'],['../UI0X03_8py.html#a563e591adc5a19d8bb7734f8b0400fbe',1,'UI0X03.ser()']]],
  ['ser_5fnum_450',['ser_num',['../classtask__share_1_1Queue.html#a6f9d87b116eb16dba0867d3746af9f5f',1,'task_share.Queue.ser_num()'],['../classtask__share_1_1Share.html#a2e8df029af46fbfd44ef0c2e7e8c7af6',1,'task_share.Share.ser_num()']]],
  ['share_5flist_451',['share_list',['../task__share_8py.html#a75818e5b662453e3723d0f234c85e519',1,'task_share']]],
  ['start_452',['start',['../main0X04__ME405_8py.html#abfb815f52013548f912c0cfbcb111e49',1,'main0X04_ME405']]],
  ['state_453',['state',['../classUI__Task_1_1UI__Task.html#aaeadcb6f1d7645638246a3d003d12470',1,'UI_Task::UI_Task']]],
  ['stop_454',['stop',['../main0X04__ME405_8py.html#aea756a6444c8af7947d97d2a1d50ab29',1,'main0X04_ME405']]],
  ['stop_5fmotors_455',['STOP_MOTORS',['../classCNTRL__Task_1_1CNTRL__Task.html#a3c1348fa75685b0398ba5c1081854bf9',1,'CNTRL_Task::CNTRL_Task']]]
];
