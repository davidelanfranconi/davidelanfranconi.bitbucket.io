var searchData=
[
  ['pc_20frontend_3a_20data_20plotting_20and_20post_2dprocessing_146',['PC Frontend: Data Plotting and Post-Processing',['../frontend_8py.html',1,'']]],
  ['pc_5ffrontend_2epy_147',['PC_Frontend.py',['../PC__Frontend_8py.html',1,'']]],
  ['period_148',['period',['../classcotask_1_1Task.html#a44f980f61f1908764c6821fa886590ca',1,'cotask.Task.period()'],['../classEncoder__Driver_1_1Encoder.html#a8e218a024e2aa9ae24f6b506b5470187',1,'Encoder_Driver.Encoder.period()']]],
  ['periods_149',['periods',['../main_8py.html#ac62c248b37fa38677883dc5f09f0a43d',1,'main']]],
  ['pina5_150',['pinA5',['../LAB0X02A__ME405_8py.html#afa88d9bda4591582315337895de0a44a',1,'LAB0X02A_ME405.pinA5()'],['../LAB0X02B__ME405_8py.html#a991d5425124048afd126431243619ce6',1,'LAB0X02B_ME405.pinA5()']]],
  ['pinb3_151',['pinB3',['../LAB0X02B__ME405_8py.html#a23ccd1cc80555659904297767f5aeba1',1,'LAB0X02B_ME405']]],
  ['pinc13_152',['pinC13',['../LAB0X02A__ME405_8py.html#ac9bc03b82391ae527b49fa8ad486bec2',1,'LAB0X02A_ME405.pinC13()'],['../main0X03__ME405_8py.html#a842851dee89c645e3ece33540a2025d5',1,'main0X03_ME405.pinC13()']]],
  ['pitchreading_153',['pitchReading',['../classIMU__Driver_1_1IMU.html#a56c0a36477ed152120304c65309cddfe',1,'IMU_Driver::IMU']]],
  ['platform_20controller_3a_154',['Platform Controller:',['../Controller_8py.html',1,'']]],
  ['platform_20user_20interface_3a_155',['Platform User Interface:',['../UI_8py.html',1,'']]],
  ['plot_5fdata_156',['Plot_Data',['../PC__Frontend_8py.html#ab38ffde908bd60742bceb587789da37f',1,'PC_Frontend']]],
  ['pngfilenamex_157',['pngFileNameX',['../PC__Frontend_8py.html#a6afc6a5193e54a9860016237410fdf57',1,'PC_Frontend']]],
  ['pngfilenamey_158',['pngFileNameY',['../PC__Frontend_8py.html#ac19b9041457b2237bd25d1b2a6850de7',1,'PC_Frontend']]],
  ['power_5freg_159',['POWER_REG',['../IMU__Driver_8py.html#a367ff4706c2363e48944cc10153cd6ce',1,'IMU_Driver']]],
  ['previouscount_160',['PreviousCount',['../classEncoder__Driver_1_1Encoder.html#a73d70b0786edb97f73c5d984f8a47769',1,'Encoder_Driver::Encoder']]],
  ['pri_5flist_161',['pri_list',['../classcotask_1_1TaskList.html#aac6e53cb4fec80455198ff85c85a4b51',1,'cotask::TaskList']]],
  ['pri_5fsched_162',['pri_sched',['../classcotask_1_1TaskList.html#a5f7b264614e8e22c28d4c1509e3f30d8',1,'cotask::TaskList']]],
  ['printreg_163',['printReg',['../classIMU__Driver_1_1IMU.html#a4922a30068806e6eaaff0dd8061fd967',1,'IMU_Driver::IMU']]],
  ['priority_164',['priority',['../classcotask_1_1Task.html#aeced93c7b7d23e33de9693d278aef88b',1,'cotask::Task']]],
  ['put_165',['put',['../classtask__share_1_1Queue.html#ae785bdf9d397d61729c22656471a81df',1,'task_share.Queue.put()'],['../classtask__share_1_1Share.html#ab449c261f259db176ffeea55ccbf5d96',1,'task_share.Share.put()']]]
];
