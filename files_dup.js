var files_dup =
[
    [ "ADCA11.py", "ADCA11_8py.html", "ADCA11_8py" ],
    [ "CNTRL_Task.py", "CNTRL__Task_8py.html", [
      [ "CNTRL_Task", "classCNTRL__Task_1_1CNTRL__Task.html", "classCNTRL__Task_1_1CNTRL__Task" ]
    ] ],
    [ "cotask.py", "cotask_8py.html", "cotask_8py" ],
    [ "Data_Task.py", "Data__Task_8py.html", "Data__Task_8py" ],
    [ "Encoder_Driver.py", "Encoder__Driver_8py.html", [
      [ "Encoder", "classEncoder__Driver_1_1Encoder.html", "classEncoder__Driver_1_1Encoder" ]
    ] ],
    [ "Encoder_Task.py", "Encoder__Task_8py.html", [
      [ "Encoder_Task", "classEncoder__Task_1_1Encoder__Task.html", "classEncoder__Task_1_1Encoder__Task" ]
    ] ],
    [ "IMU_Driver.py", "IMU__Driver_8py.html", "IMU__Driver_8py" ],
    [ "LAB0X01_ME405.py", "LAB0X01__ME405_8py.html", "LAB0X01__ME405_8py" ],
    [ "LAB0X02A_ME405.py", "LAB0X02A__ME405_8py.html", "LAB0X02A__ME405_8py" ],
    [ "LAB0X02B_ME405.py", "LAB0X02B__ME405_8py.html", "LAB0X02B__ME405_8py" ],
    [ "main.py", "main_8py.html", "main_8py" ],
    [ "main0X03_ME405.py", "main0X03__ME405_8py.html", "main0X03__ME405_8py" ],
    [ "main0X04_ME405.py", "main0X04__ME405_8py.html", "main0X04__ME405_8py" ],
    [ "MCP9808.py", "MCP9808_8py.html", "MCP9808_8py" ],
    [ "Motor_Driver.py", "Motor__Driver_8py.html", "Motor__Driver_8py" ],
    [ "Motor_Task.py", "Motor__Task_8py.html", [
      [ "Motor_Task", "classMotor__Task_1_1Motor__Task.html", "classMotor__Task_1_1Motor__Task" ]
    ] ],
    [ "PC_Frontend.py", "PC__Frontend_8py.html", "PC__Frontend_8py" ],
    [ "Shares.py", "Shares_8py.html", "Shares_8py" ],
    [ "task_share.py", "task__share_8py.html", "task__share_8py" ],
    [ "Touchpad_Driver.py", "Touchpad__Driver_8py.html", "Touchpad__Driver_8py" ],
    [ "Touchpad_Task.py", "Touchpad__Task_8py.html", [
      [ "Touchpad_Task", "classTouchpad__Task_1_1Touchpad__Task.html", "classTouchpad__Task_1_1Touchpad__Task" ]
    ] ],
    [ "UI0X03.py", "UI0X03_8py.html", "UI0X03_8py" ],
    [ "UI_Task.py", "UI__Task_8py.html", "UI__Task_8py" ]
];