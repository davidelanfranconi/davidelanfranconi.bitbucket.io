var annotated_dup =
[
    [ "ADCA11", null, [
      [ "Nucleo_Temp", "classADCA11_1_1Nucleo__Temp.html", "classADCA11_1_1Nucleo__Temp" ]
    ] ],
    [ "CNTRL_Task", null, [
      [ "CNTRL_Task", "classCNTRL__Task_1_1CNTRL__Task.html", "classCNTRL__Task_1_1CNTRL__Task" ]
    ] ],
    [ "cotask", null, [
      [ "Task", "classcotask_1_1Task.html", "classcotask_1_1Task" ],
      [ "TaskList", "classcotask_1_1TaskList.html", "classcotask_1_1TaskList" ]
    ] ],
    [ "Data_Task", null, [
      [ "Data_Task", "classData__Task_1_1Data__Task.html", "classData__Task_1_1Data__Task" ]
    ] ],
    [ "Encoder_Driver", null, [
      [ "Encoder", "classEncoder__Driver_1_1Encoder.html", "classEncoder__Driver_1_1Encoder" ]
    ] ],
    [ "Encoder_Task", null, [
      [ "Encoder_Task", "classEncoder__Task_1_1Encoder__Task.html", "classEncoder__Task_1_1Encoder__Task" ]
    ] ],
    [ "IMU_Driver", null, [
      [ "IMU", "classIMU__Driver_1_1IMU.html", "classIMU__Driver_1_1IMU" ]
    ] ],
    [ "MCP9808", null, [
      [ "MCP9808", "classMCP9808_1_1MCP9808.html", "classMCP9808_1_1MCP9808" ]
    ] ],
    [ "Motor_Driver", null, [
      [ "Motor", "classMotor__Driver_1_1Motor.html", "classMotor__Driver_1_1Motor" ]
    ] ],
    [ "Motor_Task", null, [
      [ "Motor_Task", "classMotor__Task_1_1Motor__Task.html", "classMotor__Task_1_1Motor__Task" ]
    ] ],
    [ "task_share", null, [
      [ "Queue", "classtask__share_1_1Queue.html", "classtask__share_1_1Queue" ],
      [ "Share", "classtask__share_1_1Share.html", "classtask__share_1_1Share" ]
    ] ],
    [ "Touchpad_Driver", null, [
      [ "TouchPanelDriver", "classTouchpad__Driver_1_1TouchPanelDriver.html", "classTouchpad__Driver_1_1TouchPanelDriver" ]
    ] ],
    [ "Touchpad_Task", null, [
      [ "Touchpad_Task", "classTouchpad__Task_1_1Touchpad__Task.html", "classTouchpad__Task_1_1Touchpad__Task" ]
    ] ],
    [ "UI_Task", null, [
      [ "UI_Task", "classUI__Task_1_1UI__Task.html", "classUI__Task_1_1UI__Task" ]
    ] ]
];