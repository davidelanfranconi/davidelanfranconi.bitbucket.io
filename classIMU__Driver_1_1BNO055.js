var classIMU__Driver_1_1BNO055 =
[
    [ "__init__", "classIMU__Driver_1_1BNO055.html#a50342ed015414b944011925c026d8861", null ],
    [ "calibrate", "classIMU__Driver_1_1BNO055.html#a5abbb56a52d8e12278e3696df7a92619", null ],
    [ "calibration", "classIMU__Driver_1_1BNO055.html#a54cd8df73b230769ba3c3cbc59ebca83", null ],
    [ "checkId", "classIMU__Driver_1_1BNO055.html#af1afa6b796af7dee6c73400fe9c1cbdf", null ],
    [ "configure", "classIMU__Driver_1_1BNO055.html#a32808e62af508678ee853a46954978a2", null ],
    [ "eulerPositions", "classIMU__Driver_1_1BNO055.html#ae0c790862369339e24b13395346fcbaf", null ],
    [ "pitchReading", "classIMU__Driver_1_1BNO055.html#ad1bff8c0a9c23a0400c5df22375d76b0", null ],
    [ "printReg", "classIMU__Driver_1_1BNO055.html#ac170b54c131e9d8d6d987b9b21274c1a", null ],
    [ "readBytesfromDev", "classIMU__Driver_1_1BNO055.html#acf068d90fba925d6369e9fd9ba1d3e69", null ],
    [ "readfromDev", "classIMU__Driver_1_1BNO055.html#ae0f10e3a044eeed8a1e2c5ccb004d6dd", null ],
    [ "rollReading", "classIMU__Driver_1_1BNO055.html#aa204a9b7a59dae465cc55225f429c26d", null ],
    [ "writetoDev", "classIMU__Driver_1_1BNO055.html#a57057bc8db60937e5e180fca97de39a7", null ],
    [ "address", "classIMU__Driver_1_1BNO055.html#ae7f6626fbfdcf0d447d79580612e8800", null ],
    [ "i2cObj", "classIMU__Driver_1_1BNO055.html#ad2d954cbe4a27d65f9bfdc23d25edb47", null ]
];