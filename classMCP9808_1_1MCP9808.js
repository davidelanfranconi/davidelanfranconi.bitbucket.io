var classMCP9808_1_1MCP9808 =
[
    [ "__init__", "classMCP9808_1_1MCP9808.html#aa08ef59227df7158dfe1bca77a7334f6", null ],
    [ "celsius", "classMCP9808_1_1MCP9808.html#a47dce6d776eb905464dfe6c68ade9f1a", null ],
    [ "check", "classMCP9808_1_1MCP9808.html#a85e4a979a45f5a21dcadf199fa16df9a", null ],
    [ "fahrenheit", "classMCP9808_1_1MCP9808.html#aa4f5c4d4b6ee9874f5395c5e6dd5fdc8", null ],
    [ "Address", "classMCP9808_1_1MCP9808.html#a2cd52112cdc4dc8a09e1bd3527de0a5a", null ],
    [ "buffer", "classMCP9808_1_1MCP9808.html#a06b6784b291fc99159b3e1fe9967da41", null ],
    [ "I2C", "classMCP9808_1_1MCP9808.html#a808bc0463aea8a8551868b3b74cb95a4", null ],
    [ "id", "classMCP9808_1_1MCP9808.html#aea36ba822978382e10510b485925b44e", null ]
];