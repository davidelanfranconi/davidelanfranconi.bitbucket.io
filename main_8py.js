var main_8py =
[
    [ "CNTRL_Task", "main_8py.html#a7693bea9ed73dea82c548918b74c8833", null ],
    [ "CNTRL_Task_Obj", "main_8py.html#ad1df485fc32fc4362e9dbbfbddc5fc37", null ],
    [ "Data_Task", "main_8py.html#acefc00b94f654b28be10239c8b906cd6", null ],
    [ "Encoder_Task", "main_8py.html#a92f3d5651ae0ff873427cdcafb6bb4c3", null ],
    [ "Encoder_Task_Obj", "main_8py.html#ab6a393598dd8ddcee5637d20bd011d51", null ],
    [ "kxMatrix", "main_8py.html#ad16fc39f01e44c0a539f39ad51326630", null ],
    [ "kyMatrix", "main_8py.html#a93c922a622bcee19cbc3e40f8e7e25bf", null ],
    [ "Motor_Task", "main_8py.html#a0e0b496e5a5a13f16147221855d66838", null ],
    [ "Motor_Task_Obj", "main_8py.html#adb0523240e7d5b4be84edf59e21c1883", null ],
    [ "myuart", "main_8py.html#a9be3fc12d7dd7ca0504e230b84d96936", null ],
    [ "periods", "main_8py.html#ac62c248b37fa38677883dc5f09f0a43d", null ],
    [ "Touch_Panel_Task_Obj", "main_8py.html#afe1342066025dfc109f631e317f4554b", null ],
    [ "Touchpad_Task", "main_8py.html#acd4a8ef249c7f1dff7696cd1355efbd3", null ],
    [ "UI_Task", "main_8py.html#a6a3b99158121b1ad6229ddf309390929", null ],
    [ "UI_Task_Obj", "main_8py.html#ad0fd034c4e275172f88faa288c033bab", null ]
];