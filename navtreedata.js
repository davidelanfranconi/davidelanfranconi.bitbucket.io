/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "ME 405 Portfolio", "index.html", [
    [ "Introduction", "index.html#intro", null ],
    [ "Portfolio", "index.html#portfolio", null ],
    [ "Source Code Repository", "index.html#repo", null ],
    [ "HMWK0X02: Term Project System Modeling", "HMWK0X02_ME405_8py.html", [
      [ "Summary", "HMWK0X02_ME405_8py.html#HMWK0X02_ME405_summary", null ]
    ] ],
    [ "HMWK0X04: Linearization and Simulation of System Model", "HMWK0X04_ME405_8py.html", [
      [ "Source Code", "HMWK0X04_ME405_8py.html#HMWK0X04_source", null ]
    ] ],
    [ "HMWK0X05: Pole Placement", "HMWK0X05_ME405_8py.html", [
      [ "Source Code", "HMWK0X05_ME405_8py.html#HMWK0X05_source", null ]
    ] ],
    [ "LAB0X01: Vendotron Finite State Machine", "LAB0X01_ME405_page_8py.html", [
      [ "Summary", "LAB0X01_ME405_page_8py.html#lAB0X01_summary", null ],
      [ "Design Requirements", "LAB0X01_ME405_page_8py.html#lAB0X01_design", null ],
      [ "User Inputs", "LAB0X01_ME405_page_8py.html#lAB0X01_inputs", null ],
      [ "Vendotron State Transition Diagram", "LAB0X01_ME405_page_8py.html#lAB0X01_diagram", null ],
      [ "Documentation", "LAB0X01_ME405_page_8py.html#lAB0X01_documentation", null ],
      [ "Source Code", "LAB0X01_ME405_page_8py.html#lAB0X01_source", null ]
    ] ],
    [ "LAB0X02: Think Fast!", "LAB0X02_ME405_page_8py.html", [
      [ "Summary", "LAB0X02_ME405_page_8py.html#LAB0X02_summary", null ],
      [ "Design Requirements", "LAB0X02_ME405_page_8py.html#LAB0X02_design", null ],
      [ "Overview", "LAB0X02_ME405_page_8py.html#LAB0X02_overview", null ],
      [ "Documentation", "LAB0X02_ME405_page_8py.html#LAB0X02_documentation", null ],
      [ "Source Code", "LAB0X02_ME405_page_8py.html#LAB0X02_source", null ]
    ] ],
    [ "LAB0X03: Pushing the Right Buttons", "LAB0X03_ME405_page_8py.html", [
      [ "Summary", "LAB0X03_ME405_page_8py.html#LAB0X03_summary", null ],
      [ "Design Requirements", "LAB0X03_ME405_page_8py.html#LAB0X03_design", null ],
      [ "Results", "LAB0X03_ME405_page_8py.html#LAB0X03_Voltage_Results", null ],
      [ "Time Constant Calculation", "LAB0X03_ME405_page_8py.html#LAB0X03_Time_constant", null ],
      [ "Documentation", "LAB0X03_ME405_page_8py.html#LAB0X03_documentation", null ],
      [ "Source Code", "LAB0X03_ME405_page_8py.html#LAB0X03_source", null ]
    ] ],
    [ "LAB0X04: Hot or Not?", "LAB0X04_ME405_page_8py.html", [
      [ "Summary", "LAB0X04_ME405_page_8py.html#lab0x04_summary", null ],
      [ "I2C Communications", "LAB0X04_ME405_page_8py.html#LAB0X04_I2C", null ],
      [ "Design Requirements", "LAB0X04_ME405_page_8py.html#LAB0X04_design", null ],
      [ "Temperature Results", "LAB0X04_ME405_page_8py.html#I2C_Logic_Analyzer_Signal_results", null ],
      [ "Documentation", "LAB0X04_ME405_page_8py.html#lab0x04_documentation", null ],
      [ "Source Code", "LAB0X04_ME405_page_8py.html#lab0x04_source", null ]
    ] ],
    [ "Term Project: Ball Balancing Platform", "TermProject_ME405_page_8py.html", [
      [ "Introduction", "TermProject_ME405_page_8py.html#TermProject_intro", null ],
      [ "Modeling and Theoretical Design for Platform Controller.", "TermProject_ME405_page_8py.html#TermProject_Modeling", null ],
      [ "Ball Balancing Platform Control and Driver Sections", "TermProject_ME405_page_8py.html#TermProject_Files", null ],
      [ "Source Code", "TermProject_ME405_page_8py.html#TermProject_source", null ]
    ] ],
    [ "Touchpad Driver:", "Touchpad_Driver_page_8py.html", [
      [ "Summary", "Touchpad_Driver_page_8py.html#Touchpad_summary", null ],
      [ "Reading from Resistive Touch Panels", "Touchpad_Driver_page_8py.html#Touchpad_instructions", null ],
      [ "Design Requirements", "Touchpad_Driver_page_8py.html#Touchpad_Design_Requirements", null ],
      [ "Touchpad Testing and Code Validation", "Touchpad_Driver_page_8py.html#Touchpad_verification", null ],
      [ "Documentation", "Touchpad_Driver_page_8py.html#Touchpad_documentation", null ],
      [ "Source Code", "Touchpad_Driver_page_8py.html#Touchpad_Driver_source", null ]
    ] ],
    [ "Motor Driver:", "Motor_Driver_page_8py.html", [
      [ "Summary", "Motor_Driver_page_8py.html#Motor_summary", null ],
      [ "Driving DC Motors with PWM and H-Bridges", "Motor_Driver_page_8py.html#Motor_instructions", null ],
      [ "Design Requirements", "Motor_Driver_page_8py.html#Motor_Design_Requirements", null ],
      [ "Motor Testing and Code Validation", "Motor_Driver_page_8py.html#Motor_verification", null ],
      [ "Documentation", "Motor_Driver_page_8py.html#Motor_documentation", null ],
      [ "Source Code", "Motor_Driver_page_8py.html#Motor_Driver_source", null ]
    ] ],
    [ "Encoder Driver:", "Encoder_Driver_page_8py.html", [
      [ "Summary", "Encoder_Driver_page_8py.html#Encoder_summary", null ],
      [ "Reading from Quadrature Encoders", "Encoder_Driver_page_8py.html#Encoder_instructions", null ],
      [ "Design Requirements", "Encoder_Driver_page_8py.html#Encoder_Design_Requirements", null ],
      [ "Encoder Testing and Code Validation", "Encoder_Driver_page_8py.html#Encoder_verification", null ],
      [ "Encoder Live Demo", "Encoder_Driver_page_8py.html#Encoder_LiveDemo", null ],
      [ "Documentation", "Encoder_Driver_page_8py.html#Encoder_documentation", null ],
      [ "Source Code", "Encoder_Driver_page_8py.html#Encoder_Driver_source", null ]
    ] ],
    [ "Inertial Measurement Unit Driver:", "IMU_Driver_page_8py.html", [
      [ "Summary", "IMU_Driver_page_8py.html#IMU_summary", null ],
      [ "Reading from Quadrature Encoders", "IMU_Driver_page_8py.html#IMU_instructions", null ],
      [ "Design Requirements", "IMU_Driver_page_8py.html#IMU_Design_Requirements", null ],
      [ "IMU Testing and Code Validation", "IMU_Driver_page_8py.html#IMU_verification", null ],
      [ "Inertial Measurement Unit Live Demo", "IMU_Driver_page_8py.html#IMU_LiveDemo", null ],
      [ "Documentation", "IMU_Driver_page_8py.html#IMU_documentation", null ],
      [ "Source Code", "IMU_Driver_page_8py.html#IMU_Driver_source", null ]
    ] ],
    [ "Platform User Interface:", "UI_8py.html", [
      [ "UI Overview", "UI_8py.html#UI_overview", null ],
      [ "Documentation", "UI_8py.html#UI_documentation", null ],
      [ "Source Code", "UI_8py.html#UI_Driver_source", null ]
    ] ],
    [ "Platform Controller:", "Controller_8py.html", [
      [ "Controller Overview", "Controller_8py.html#controller_overview", null ],
      [ "Documentation", "Controller_8py.html#controller_documentation", null ],
      [ "Source Code", "Controller_8py.html#controller_source", null ]
    ] ],
    [ "Task Scheduler:", "Task_Scheduler_8py.html", [
      [ "Scheduler Overview", "Task_Scheduler_8py.html#scheduler_overview", null ],
      [ "Finite State Diagram for the Ball Balancing Platform.", "Task_Scheduler_8py.html#scheduler_FSM", null ],
      [ "Co-Task Scheduler Timing Management", "Task_Scheduler_8py.html#scheduler_timing", null ],
      [ "Documentation", "Task_Scheduler_8py.html#scheduler_documentation", null ],
      [ "Source Code", "Task_Scheduler_8py.html#scheduler_source", null ]
    ] ],
    [ "Data Sharing Across Tasks:", "shares_8py.html", [
      [ "Shares Variable/Command Sharing Overview", "shares_8py.html#shares_overview", null ],
      [ "Reading and Writing to Variables", "shares_8py.html#shares_rd_wt", null ],
      [ "Accessing Queues", "shares_8py.html#shares_queues", null ],
      [ "Documentation", "shares_8py.html#shares_documentation", null ],
      [ "Source Code", "shares_8py.html#shares_source", null ]
    ] ],
    [ "Data Collection Across Tasks and programs", "data_task_8py.html", [
      [ "Collecting Data and Sending it to the PC for Post-Processing", "data_task_8py.html#data_Collection", null ],
      [ "Documentation", "data_task_8py.html#data_documentation", null ],
      [ "Source Code", "data_task_8py.html#data_source", null ]
    ] ],
    [ "PC Frontend: Data Plotting and Post-Processing", "frontend_8py.html", [
      [ "Overview", "frontend_8py.html#frontend_Overview", null ],
      [ "Documentation", "frontend_8py.html#frontend_documentation", null ],
      [ "Source Code", "frontend_8py.html#frontend_source", null ]
    ] ],
    [ "Final Results: Project Results and Reflection", "final_results_8py.html", [
      [ "Project Results and Findings", "final_results_8py.html#final_results", null ],
      [ "Term Project Live Demonstration", "final_results_8py.html#TermProject_Demo", null ],
      [ "Term Project Conclusion and Reflection", "final_results_8py.html#final_reflection", null ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"ADCA11_8py.html",
"classEncoder__Driver_1_1Encoder.html#ad7ec3cfe23fb8330fb1e8a144cd19323"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';